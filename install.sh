#!/bin/bash
echo "Build Number $1"
echo "Running kubernetes deployments"
kubectl apply -f dataVolume.yml
kubectl apply -f postgres.yml
kubectl apply -f devops_migration.yml
kubectl apply -f devops_backend_app.yml
echo "Launched deployments"
if [ "$#" -ge 1 ]; then
  echo "Updating images of the deployments"
  kubectl set image deployment/devops-backend-app devops-backend-app=naumanurrehman/devops_backend_app:"$1"
  kubectl set image pod/devops-migration devops-migration=naumanurrehman/devops_migration:"$1"
  echo "Updated images of the deployments"
fi

