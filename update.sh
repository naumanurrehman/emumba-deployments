#!/bin/bash
echo "Running kubernetes deployments"
kubectl get pvc/postgres-pv-claim
if ![$?]; then
  kubectl create -f dataVolume.yml
fi
kubectl get service/postgres
if ![$?]; then
  kubectl create -f postgres.yml
fi
kubectl get pods/devops_migration
if ![$?]; then
  kubectl create -f devops_migration.yml
fi
kubectl get deployment/devops_backend_app
if ![$?]; then
  kubectl create -f devops_backend_app.yml
fi
echo "Launched deployments"

