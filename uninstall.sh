#!/bin/bash
echo "Deleting kubernetes deployments"
kubectl delete -f devops_backend_app.yml
kubectl delete -f devops_migration.yml
kubectl delete -f postgres.yml
kubectl delete -f dataVolume.yml
echo "Deleted deployments"

